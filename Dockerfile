FROM python:3.7

RUN mkdir /app
WORKDIR /app
ADD . /app/
RUN pip install -r requirements.txt

HEALTHCHECK CMD curl --fail http://localhost:5000 || exit 1
EXPOSE 5000
CMD ["python", "/app/main.py"]
